package com.jsw.trabajosobreficheros;

import android.accounts.NetworkErrorException;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.entity.BufferedHttpEntity;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

/**
 * Created by joselu on 30/11/16.
 */

public class Descarga extends AsyncTask<String, String, ArrayList>{

    public List<String> getStrings(String url){
        return this.doInBackground(url);
    }

    @Override
    protected ArrayList doInBackground(String... URL){

        ArrayList<String> res = new ArrayList<String>();

        try {
            DefaultHttpClient httpclient = new DefaultHttpClient();
            HttpGet httppost = new HttpGet(URL[0].toString());
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity ht = response.getEntity();

            BufferedHttpEntity buf = new BufferedHttpEntity(ht);

            InputStream is = buf.getContent();


            BufferedReader r = new BufferedReader(new InputStreamReader(is));

            String line;
            while ((line = r.readLine()) != null) {
                res.add(line);
            }
        } catch(IOException ex){
            Log.d("EXCEPCION", ex.getMessage());
            setError();
        }

        return res;
    }

    public long getInterval(String URL){
        long res;

        try{
            String linea = "";
            File f = new File(URL);

            BufferedReader bf = new BufferedReader(new FileReader(f));
            while ((linea = bf.readLine()) != null){
                continue;
            }

            res = Long.parseLong(linea);

        } catch (Exception ex){
            res = 4000;
        }

        return res;
    }

    public void setError(){
        String URL = "https://acta.000webhostapp.com/errors.php";
        HttpClient client = new DefaultHttpClient();

        try {
            client.execute(new HttpGet(URL));
        } catch(IOException e) {
            //do something here
        }
    }
}
