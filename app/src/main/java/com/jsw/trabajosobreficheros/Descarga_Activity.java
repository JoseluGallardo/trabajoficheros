package com.jsw.trabajosobreficheros;

import android.app.ProgressDialog;
import android.media.Image;
import android.os.CountDownTimer;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.FileReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static android.R.id.list;

public class Descarga_Activity extends AppCompatActivity {

    private TextInputLayout mImage, mFrase, mIntervalo;
    private ImageView mImagen;
    private String mLink;
    private List<String> images;
    private List<String> frases;
    private long intervalo = 0;
    private TextView mText;
    private int iterator = 0;
    private CountDownTimer reloj;
    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descarga);
        mImage = (TextInputLayout) findViewById(R.id.til_imagenes);
        mFrase = (TextInputLayout) findViewById(R.id.til_frases);
        mIntervalo = (TextInputLayout) findViewById(R.id.til_intervalo);
        mImagen = (ImageView) findViewById(R.id.iv_imagen);
        mText = (TextView)findViewById(R.id.tv_frase);
        pd = new ProgressDialog(this);
        reloj = new CountDownTimer(4000, 4000) {
            @Override
            public void onTick(long l) {
                mostrar();
            }

            @Override
            public void onFinish() {
                reloj.start();
            }
        };

        images = new ArrayList<>();
        frases = new ArrayList<>();
    }

    private void picasso(String link){
        Picasso.with(getApplicationContext()).load(link) .placeholder(R.drawable.wait) .error(R.drawable.error)
                .resize(300, 300).into(mImagen);
    }

    public void descargar(View v){
        try{
            images.addAll(new Descarga().execute(new String[]{mImage.getEditText().getText().toString()}).get());
            frases.addAll(new Descarga().execute(new String[]{mFrase.getEditText().getText().toString()}).get());
            intervalo = new Descarga().getInterval(mIntervalo.getEditText().getText().toString());
            reloj.start();
        }
        catch(Exception ex){
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void mostrar(){
        try {
            iterator++;
            picasso(images.get(iterator % images.size()));
            mText.setText(frases.get(iterator % frases.size()));
        } catch(Exception ex){
            mText.setText("Frase no localizada. ¿Error?");
        }
    }
}
